package pages;






 

import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


public class TestPage extends BasePage{

	final WebDriver driver;
	public TestPage(WebDriver driver){
		this.driver = driver;  
		PageFactory.initElements(driver, this);
	} 	
	
	
	/*
	 ******PASAR A BASEPAGE
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "btnContinuar")
	private WebElement btnContinuar;

	@FindBy(how = How.ID,using = "btnPago")
	private WebElement btnPago;


	
	//*****************************


	@FindBy(how = How.ID,using = "nombre")
	private WebElement nombre;

	@FindBy(how = How.ID,using = "tipoDoc")
	private WebElement tipoDoc;

	@FindBy(how = How.ID,using = "numeroDoc")
	private WebElement numeroDoc;

	@FindBy(how = How.ID,using = "email")
	private WebElement email; 

	@FindBy(how = How.ID,using = "direccion")
	private WebElement direccion; 

	@FindBy(how = How.ID,using = "piso")
	private WebElement piso; 

	@FindBy(how = How.ID,using = "depto")
	private WebElement depto; 

	@FindBy(how = How.ID,using = "observaciones")
	private WebElement observaciones; 

	@FindBy(how = How.ID,using = "localidad")
	private WebElement localidad; 

	@FindBy(how = How.ID,using = "codPostal")
	private WebElement codPostal;


	//----TRAMITE ABM 

	
	
	@FindBy(how = How.ID,using = "btnAceptar")
	private WebElement btnAceptar;
	
	@FindBy(how = How.ID,using = "btnAceptar")
	private WebElement brtEDITAR;
	
	
	

	@FindBy(how = How.XPATH,using = "//*[@id='ctl00_CPH1_lstEncuadreLegal']/option")
	private WebElement encuadreLegal;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'ctl00_CPH1_lstMonedas\']/option")
	private WebElement pesosArgentinos;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\"ctl00_CPH1_lstMonedas\"]/option")
	private WebElement pesoArgentino;
	
	@FindBy(how = How.XPATH,using = "//*[@id='ctl00_CPH1_supervisorUC_lsbUsuarios']/option[2]")
	private WebElement supervisorOfeliaDieguez;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'ctl00_CPH1_gridResultado_ctl02_ucAccionesBuscadorGrid_modalBody\']/li[2]/a")
	private WebElement editarProceso;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'ctl00_CPH1_tmHoraActoApertura_txtHoraMinuto\']")
	private WebElement horaInicialRecepcion;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'ctl00_CPH1_modalBody\']/li/a")
	private WebElement adquirirPliego;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\"ctl00_CPH1_modalBody\"]/li/a[contains(.,'Ofertar')]")
	private WebElement btnOFERTAR;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'ctl00_CPH1_UCItemsInformacionEconomica_gvItemPliego_ctl02_divBddAcciones\']/button")
	private WebElement btnAccionesPliego;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'ctl00_CPH1_UCItemsInformacionEconomica_gvItemPliego_ctl02_lnkOfertar\']")
	private WebElement btnOfertarPliego;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'ctl00_CPH1_gridResultado_ctl02_ucAccionesBuscadorGrid_modalBody\']/li[2]/a")
	private WebElement aperturaDeOferta;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'ctl00_CPH1_modalBody\']/li[1]/p/a")
	private WebElement evaluacionAdministrativa;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'ctl00_CPH1_modalBody\']/li[2]/p/a")
	private WebElement evaluacionTecnica;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\'ctl00_CPH1_modalBody\']/li[3]/p/a")
	private WebElement evaluacionEconomica;
	
	
	
	//----TRAMITE

	/*
	 **************METODOS***************** 
	 */
		
	public void btnAceptar() {
		btnAceptar.click();
	}

/*
 	MAU PRACTICE AREA
 */
	
	
	/*IDs BAC MAU Ingreso*/ 
	
	@FindBy(how = How.ID,using ="ctl00_CtrlMenuPortal_lnkIngresar")
	private WebElement btnIngresarPortal;
	
	@FindBy(how = How.ID,using ="ctl00_CtrlMenuPortal_logIn_txtUsername_txtTextBox")
	private WebElement nombreUsuario;
	
	@FindBy(how = How.ID,using ="ctl00_CtrlMenuPortal_logIn_txtPassword_txtTextBox")
	private WebElement passUsuario;
	
	@FindBy(how = How.ID,using ="ctl00_CtrlMenuPortal_logIn_btnIngresar")
	private WebElement btnIngresar;

	@FindBy(how = How.ID,using ="ctl00_CPH1_txtNomProceso")
	private WebElement nombreProcesoDeCompra;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_rblLicitConcursoCont_2")
	private WebElement licitacionPublica;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_chkCM")
	private WebElement convenioMarco;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_imgAgregarSeleccionado")
	private WebElement agregatItemSeleccionado;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_lnkSiguientePaso")
	private WebElement btnSiguientePaso;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_seccionIndice1_hlDatos")
	private WebElement informacionBasicaDeProceso;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_rblTipoEtapa_0")
	private WebElement etapaUnica;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_rblTipoCotizCantRenglonAdjudicacion_1")
	private WebElement porCantidadDeRenglonTOTAL;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_rblTipoCotizRenglonAdjudicacion_1")
	private WebElement porRenglonesTOTAL;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_rblInvitacionProveedor_0")
	private WebElement formaDeEnvioDeInvitacionesGENERAL;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtTelefonoContactoUOA")
	private WebElement TelefonoDeContactoUOA;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_lnkGuardarYVolver")
	private WebElement btnGuardarYvolver;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_seccionIndice15_hlDatos")
	private WebElement detalleDeProductosOservicios;

	@FindBy(how = How.ID,using ="ctl00_CPH1_txtCodigoItem")
	private WebElement codigoDeItem;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_lnkIngresarItem")
	private WebElement cargarItem;

	@FindBy(how = How.ID,using ="ctl00_CPH1_seccionIndice6_hlDatos")
	private WebElement RequisitosMinimosDeParticipacion;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_seccionIndice9_hlDatos")
	private WebElement garantias;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_seccionIndice10_hlDatos")
	private WebElement montoYduracionDelContrato;
	
	@FindBy(how = How.ID,using ="ctl00_CtrlHeader_lnkCerrarSesion")
	private WebElement btnCerrarCesion;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtNumProceso")
	private WebElement buscarProcesoDeCompra;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_btnVer")
	private WebElement btnBucarProcesoDeCompra;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_gridResultado_ctl02_ucAccionesBuscadorGrid_lnkCalcularAcciones")
	private WebElement btnAcciones;
	
	@FindBy(how = How.ID,using ="txtNroPliegoPublicado")
	private WebElement estadoPliegoPublicado;
	
	@FindBy(how = How.ID,using ="btnPliegoPublicar")
	private WebElement btnPublicarPliego;
	
	@FindBy(how = How.ID,using ="ctl00_NavBar_CtrlmenubaseBastrap_lnkEscritorio")
	private WebElement btnMiEscritorio;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_imgAgregarMoneda")
	private WebElement tildeVerde;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtDuracionConvenio")
	private WebElement DuracionConvenio;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ddlTipoDuracionConvenio")
	private WebElement DuracionConvenioDesplegable;
	
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ddlDirDoctoFisica")
	private WebElement direccionDesplegable;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtAcondicionamiento")
	private WebElement descripcionAcondicionamiento;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtCantidad")
	private WebElement cantidadDeItems;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_lnkAgregarItem")
	private WebElement btnAgregarItems;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_lnkGuardarVolver")
	private WebElement btnGuardarVolver;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_RE_1_btnEliminar")
	private WebElement btnEliminarRequisito1;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_RT_2_btnEliminar")
	private WebElement btnEliminarRequisito2;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_RA_3_btnEliminar")
	private WebElement btnEliminarRequisito3;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtPorcGarantiaImpugnacion")
	private WebElement porcentajeDeGarantias;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_rdoGarantiaOfertaCompraMenorNo")
	private WebElement garantiaDeMantenimientoDeOfertaNO;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_rdoCumplimientoContratoNo")
	private WebElement garantiaDeCumplimientoDeContratoNO;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_rdoContragarantiaNo")
	private WebElement ContragarantiaNo;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_imgAgregarMoneda")
	private WebElement btnAgregarMoneda;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_UCDatosInformacionContrato_txtMontoContrato_I")
	private WebElement montoContrato;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_UCDatosInformacionContrato_rdbDesdePerfeccionamiento")
	private WebElement checkDesdePerfeccionamiento;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_UCDatosInformacionContrato_txtDiasDuracionContrato")
	private WebElement duracionDelContrato;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_UCDatosInformacionContrato_ddlDuracionContrato")
	private WebElement DuracionDelContratoDesplegable;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_SeccionIngresoSupervisor_hlDatos")
	private WebElement itemSupervisor10;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_supervisorUC_btnAgregar")
	private WebElement btnAgregarSupervisor;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_seccionIndice17_hlDatos")
	private WebElement itemEvaluadores14;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_GridEvaluadoresDisponibles_ctl03_chkAgregarUsuario")
	private WebElement evaluadorPabloLucasConverso;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_GridEvaluadoresDisponibles_ctl04_chkAgregarUsuario")
	private WebElement evaluadorFranciscoCiorciari;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_GridEvaluadoresDisponibles_ctl05_chkAgregarUsuario")
	private WebElement evaluadorAngelaCiorciari;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_btnAgregarEvaluador")
	private WebElement btnAgregarEvaluador;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_GridEvaluadores_ctl02_divBddAcciones")
	private WebElement btnAccionesPcompra;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_GridEvaluadores_ctl02_lnkIngresarActa")
	private WebElement btnIngresarActa;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucActoAdministrativoSADE_ddlTipoDocumento")
	private WebElement tipoDocumentoDesplegable;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucActoAdministrativoSADE_txtAnioDocumento")
	private WebElement ejercicioDocumento;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucActoAdministrativoSADE_txtNumero")
	private WebElement numeroDocumento;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucActoAdministrativoSADE_txtReparticion")
	private WebElement reparticionMGEYA;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucActoAdministrativoSADE_btnIngresarDocumento")
	private WebElement btnIngresarDocumento;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_GridEvaluadores_ctl03_divBddAcciones")
	private WebElement btnAccionesPcompra2;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_GridEvaluadores_ctl03_lnkIngresarActa")
	private WebElement btnIngresarActa2;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_GridEvaluadores_ctl04_divBddAcciones")
	private WebElement btnAccionesPcompra3;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_GridEvaluadores_ctl04_lnkIngresarActa")
	private WebElement btnIngresarActa3;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_btnGuardar")
	private WebElement btnGuardar;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_seccionIndiceAlcance_hlDatos")
	private WebElement alcanceITEM;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_chkTodoGCBA")
	private WebElement checkTodoGCBA;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_lnkFinalizar")
	private WebElement btnFinalizar;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_seccionIndiceSustentabilidad_hlDatos")
	private WebElement seccionSustentabilidad;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_lnkEnviarSupervisor")
	private WebElement btnEnviarAlSupervisor;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_usrCabeceraPliego_lblNumPliego")
	private WebElement nroDeSGgenerado;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_seccionIndiceAutorizacion_hlDatos")
	private WebElement administrativoAutorizacion;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucIngresarActo_ddlTipoDocumento")
	private WebElement tipoDNI2;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucIngresarActo_DropDownAnio")
	private WebElement nroEjercicio2;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucIngresarActo_txtReparticion")
	private WebElement reparticionMGEYA2;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucIngresarActo_txtNumero")
	private WebElement nroReparticion2;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucIngresarActo_rbNumeroSADE")
	private WebElement nroSADE;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucIngresarActo_btnBuscar")
	private WebElement btnBuscarActo;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucIngresarActo_rptActoAdministrativo_ctl01_divBddAcciones")
	private WebElement btnAcciones1;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucIngresarActo_rptActoAdministrativo_ctl01_btnVincular")
	private WebElement btnVincular1;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ucIngresarActo_btnIngresarActo")
	private WebElement btnIngresarActo;
	
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_seccionIndice4_hlDatos")
	private WebElement cronogramaItem;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_dxedteEstimadaPublicacion_I")
	private WebElement estimadaPublicacion;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_tmHoraEstimadaPublicacion_txtHoraMinuto")
	private WebElement horaEstimadaPublicacion;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_dxedteInicioConsultas_I")
	private WebElement FechaInicioDeConsultas;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_tmHoraInicioConsultas_txtHoraMinuto")
	private WebElement horaInicioDeConsultas;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_dxedteFinalConsultas_I")
	private WebElement FechaFinalDeConsultas;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_tmHoraFinalConsultas_txtHoraMinuto")
	private WebElement horaFinalDeConsultas;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_dxedteActoApertura_I")
	private WebElement FechaFinalDerecepcionDeOfertas;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_tmHoraActoApertura_txtHoraMinuto")
	private WebElement horaFinalDerecepcionDeOfertas;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_dxedteInicioSoporte_I")
	private WebElement fechaRecepcionDeDocumentacionFisica;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_tmHoraInicioSoporte_txtHoraMinuto")
	private WebElement horaRecepcionDeDocumentacionFisica;
	
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_dxedteFinalSoporte_I")
	private WebElement fechaFinalDeRecepcionDeDocumentacionFisica;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_tmHoraFinalSoporte_txtHoraMinuto")
	private WebElement horaFinalDeRecepcionDeDocumentacionFisica;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_seccionIndiceLlamado_hlDatos")
	private WebElement actoAdministrativoAlLlamado;
	
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_btnBusquedaRapida")
	private WebElement btnBusquedaRapida;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_GridListaPliegos_ctl02_lnkAcciones")
	private WebElement btnAcciones2;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_lnkAdquirir")
	private WebElement adquirirPliegoID;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtPrecioUnitario_txtTextBox_txtTextBox")
	private WebElement precioUnitario;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtEspecificacionesTecnicas")
	private WebElement EspecificacionesTecnicas;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_lnkGuardar")
	private WebElement btnGUARDAR;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_chkAceptoCondiciones")
	private WebElement chkAceptoCondiciones;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_lnkConfirmar")
	private WebElement lnkConfirmar;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtNombreOferta")
	private WebElement nombreDeLaOferta;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtDescripcionOferta")
	private WebElement descripcionDeLaOferta;
	
	@FindBy(how = How.ID,using ="txtNroPliegoEnApertura")
	private WebElement PliegoEnApertura;
	
	@FindBy(how = How.ID,using ="btnPliegoEnApertura")
	private WebElement btnPliegoEnApertura;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_gvListaOfertas_ctl02_lnkOpciones")
	private WebElement btnOpciones;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_rbtEstadoInscripcionSi")
	private WebElement EstadoInscripcionSi;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_lnkFinalizarEvaluacion")								 
	private WebElement FinalizarEvaluacion;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_gvCriteriosporRenglon_cell0_7_RadioAccion_0")
	private WebElement btnCheckCumple;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_lnkFinalizaEvaluacion")								 
	private WebElement FinalizaEvaluacion;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_grid_EvaRengloPliego_cell0_8_RadioAccion_0")								 
	private WebElement btnCumplebtn;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_chkConvencional")								 
	private WebElement tipoDeConvenioMarco;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtMontoEstimado")								 
	private WebElement MontoEstimado;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_ddlTipoDuracionOferta")
	private WebElement tipoDeDiasDesplegable;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtPeriodicidadCada")
	private WebElement PeriodicidadCada;
	
	@FindBy(how = How.ID,using ="ctl00_CPH1_txtDiasPeriodoActualizacion")
	private WebElement PeriodoActualizacion;
	
	
	
	
	/*BAC-convenio-Marco-Mau*/
	
	public void circuitoConvenioMarco()
	{ 
		btnIngresarPortal.click();//Btn Ingresar
		nombreUsuario.sendKeys("odieguez ");//nombre usuario
		passUsuario.sendKeys("12345678");//contraseña usuario
		btnIngresar.click();//confirmar ingreso de usuario
	}

	public void crearPdeCompra(String crearBAC, String cPDeCompra)
	{
		List<WebElement> btnCrearBAC = driver.findElements(By.tagName("li")); 
        int i;
        	outerloop:
        		for(i = 0; i < btnCrearBAC.size(); i++)
        	{ 
        			if((btnCrearBAC.get(i).getText().toLowerCase()).contains(crearBAC.toLowerCase()))
        				{                
        					btnCrearBAC.get(i).click();                	
                                             
        					List<WebElement> subListaCrearBAC = btnCrearBAC.get(i).findElements(By.tagName("li"));
        						for(i = 0; i < subListaCrearBAC.size(); i++)            	
        						{             	
        							if((subListaCrearBAC.get(i).getText().toLowerCase()).contains(cPDeCompra.toLowerCase()))            		
        							{                     
        								subListaCrearBAC.get(i).click(); 
        								espera(3000);
        								break outerloop;
        							}
        						}
        				}
        	}
	}

	public void pDeCompraStep1()
	{
		nombreProcesoDeCompra.sendKeys("Nombre de Prueba Convenio MARCO");
		licitacionPublica.click();//tipo de licitacion
		espera(1000);
		convenioMarco.click();
		espera(1000);
		encuadreLegal.click();//seleccion de encuadrelegal
		agregatItemSeleccionado.click();//btn con forma de tilde verde
		espera(1000);
		tipoDeConvenioMarco.click();//convencional
		espera(1000);
		btnSiguientePaso.click();
		espera(40000);

		try {
		    		waitForElementToDisappear(By.id("divPanelFondoTransparente"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		
	}
	
	public void pDeCompraStep2A()
	{
		espera(3000);
		informacionBasicaDeProceso.click();
		espera(3000); 
		porCantidadDeRenglonTOTAL.click();
		porRenglonesTOTAL.click();
		pesosArgentinos.click();//PESOS ARGENTINOS
		tildeVerde.click();//btn Agregar
		espera(1000);
		MontoEstimado.sendKeys("1000");
		DuracionConvenio.sendKeys("120");
		
		WebElement TipoDeDias = DuracionConvenioDesplegable;//duracion de convenio
		Select dropdown = new Select(TipoDeDias);
			   dropdown.selectByVisibleText("Días hábiles"); 
			   espera(3000);
		
		formaDeEnvioDeInvitacionesGENERAL.click();
		espera(1000);
		
		WebElement TipoDeDias2 = tipoDeDiasDesplegable;//tipo de dias ITEM desplegable
		Select dropdown2 = new Select(TipoDeDias2);
			   dropdown2.selectByVisibleText("Días hábiles"); 
			   espera(3000);
		
		WebElement direccion = direccionDesplegable;//Direccion ITEM DESPLEGABLE
		Select dropdown1 = new Select(direccion);
			   dropdown1.selectByVisibleText("Rivadavia 524, 1º Piso"); 
			   espera(3000);
			   
		TelefonoDeContactoUOA.sendKeys("123456789");
		PeriodicidadCada.sendKeys("365");
		PeriodoActualizacion.sendKeys("365");
		btnGuardarYvolver.click();
		espera(3000);
		
	}

	public void pDeCompraStep2B()
	{
		detalleDeProductosOservicios.click();
		codigoDeItem.sendKeys("33.10.001.0005.7");
		cargarItem.click();
		espera(3000);
		descripcionAcondicionamiento.sendKeys("123");//ACONDICIONAMIENTO
		cantidadDeItems.sendKeys("10");//CANTIDAD
		btnAgregarItems.click();//btn Agregar Item
		espera(1000);
		btnGuardarVolver.click();//btn guardar y volver
		espera(10000);
		
	}

	public void pDeCompraStep2C()
	{
		RequisitosMinimosDeParticipacion.click();
		
		//eliminacion del primer requisito
		
		espera(3000);

	driver.findElement(By.id("ctl00_CPH1_RE_1_txtDescripcion")).sendKeys("texto libre");
		
	/*		
		btnEliminarRequisito1.click();
		 	espera(1000);
		 	Alert alert = driver.switchTo().alert();
		 	alert.accept(); 
	*/	 
		//eliminacion del segundo requisito
		
		btnEliminarRequisito2.click();
		 	espera(1000);
		  	Alert alert2 = driver.switchTo().alert();
		 	alert2.accept(); 
		 
		//eliminacion del tercer requisito
		
		btnEliminarRequisito3.click();
		 	espera(1000);
		 	Alert alert3 = driver.switchTo().alert();
		 	alert3.accept(); 
		 	
	 	btnGuardarYvolver.click();
	}

	public void ClausulasParticulares() 
	{
		driver.findElement(By.id("ctl00_CPH1_seccionIndice7_hlDatos")).click();
		
		 WebElement listaSelect = tipoDNI2;// TIPO DNI
			Select dropdown = new Select(listaSelect);
				   dropdown.selectByVisibleText("Informe"); 
			espera(1000);
			
		
		
		WebElement listaSelect1 = nroEjercicio2;// EJERCICIO
			Select dropdown1 = new Select(listaSelect1);
				   dropdown1.selectByVisibleText("2015"); 
			espera(1000);
			
		
		reparticionMGEYA2.sendKeys("MGEYA");//Reparticion
		nroReparticion2.sendKeys("03572206");//Numero
		
		nroSADE.click();//seleccionar SADE
		
		btnBuscarActo.click();//btn buscar
		espera(5000);
		
		btnAcciones1.click();//btn opciones/acciones
		espera(1000);
		
		btnVincular1.click();//vincular
		espera(5000);
		
		btnIngresarActo.click();//btn finalizar
		espera(60000); 
			
		}
	
	public void pDeCompraStep2D()
	{
		garantias.click();
		espera(3000);
		
		porcentajeDeGarantias.sendKeys("1");
		garantiaDeMantenimientoDeOfertaNO.click();
		garantiaDeCumplimientoDeContratoNO.click();//garantia de cumplimiento de trabajo NO
		ContragarantiaNo.click();//La garantía de cumplimiento de contrato debe ser no menor del diez por ciento (10%) sobre el valor total de la adjudicación.
		 
		pesoArgentino.click();//MONEDA PESO ARGENTINO
		espera(1000);
		btnAgregarMoneda.click();//btn agregar moneda
		espera(1000);
		btnGuardarYvolver.click();
	}
	
	public void pDeCompraStep2E()
	{
		/*Seleccion de supervisor*/
		itemSupervisor10.click();//item 10 SUPERVISOR
		 espera(3000);
		 
		 supervisorOfeliaDieguez.click();//seleccion de supervisor Ofelia Dieguez
		 espera(1000);
		 btnAgregarSupervisor.click();//btn agregar
		 espera(500);
		 
		 btnGuardarYvolver.click(); 
		 espera(3000);
	}

	public void pDeCompraStep2F()
	{
		 /*EVALUADORES*/
		 
		itemEvaluadores14.click();//item 14 Evaluadores
		 espera(3000);
		 
		 evaluadorPabloLucasConverso.click();//Pablo Lucas Converso
		 evaluadorFranciscoCiorciari.click();//Francisco Ciorciari
		 //evaluadorAngelaCiorciari.click();//Angela Ciorciari
		 driver.findElement(By.id("ctl00_CPH1_GridEvaluadoresDisponibles_ctl11_chkAgregarUsuario")).click();
		 
		 btnAgregarEvaluador.click();//btn AGREGAR 
		 espera(3000);
		 
		 btnAccionesPcompra.click();
		 btnIngresarActa.click();
		 espera(2000);
		 
		 WebElement listaSelect = tipoDocumentoDesplegable;// TIPO DNI
			Select dropdown = new Select(listaSelect);
				   dropdown.selectByVisibleText("Declaración Jurada Incompatibilidad C. Evaluadora");		 
				   espera(1000);
				
		ejercicioDocumento.sendKeys("2015");
		numeroDocumento.sendKeys("03572206");
		reparticionMGEYA.sendKeys("MGEYA");
		btnIngresarDocumento.click();//btn ingresar
		espera(3000);
		 
		btnAccionesPcompra2.click();
		btnIngresarActa2.click();
		espera(2000);
		 
		WebElement listaSelect2 = tipoDocumentoDesplegable;// TIPO DNI
			Select dropdown2 = new Select(listaSelect2);
				   espera(1000);
				   dropdown2.selectByVisibleText("Declaración Jurada Incompatibilidad C. Evaluadora"); 
				
		ejercicioDocumento.sendKeys("2015");
		numeroDocumento.sendKeys("03572206");
		reparticionMGEYA.sendKeys("MGEYA");
		btnIngresarDocumento.click();//btn ingresar
		espera(3000); 
		 
		btnAccionesPcompra3.click();
		btnIngresarActa3.click();
		espera(2000); 
	
		WebElement listaSelect3 = tipoDocumentoDesplegable;// TIPO DNI
			Select dropdown3 = new Select(listaSelect3);
				   espera(1000);
				   dropdown3.selectByVisibleText("Declaración Jurada Incompatibilidad C. Evaluadora"); 
				
		ejercicioDocumento.sendKeys("2015");
		numeroDocumento.sendKeys("03572206");
		reparticionMGEYA.sendKeys("MGEYA");
		btnIngresarDocumento.click();//btn ingresar
		espera(3000); 
		
		
		btnGuardar.click();//btn guardar y volver
		 espera(5000);
	}

	public void pDeCompraStep2G()
	{
		//Sustentabilidad
		seccionSustentabilidad.click();
		espera(3000);
		btnGuardarYvolver.click();//btn guardar y volver
		espera(3000);
	}
	
	public void enviarAlSupervisor()
	{
		btnEnviarAlSupervisor.click();//btn enviar al supervisor
		espera(30000);
	}

	public String nroDeLPUBAC()
	 {
			
			WebElement nroCompraEnBac = nroDeSGgenerado;//levanta el nro de la SG generada
			String nroCompraEnBacLPU = nroCompraEnBac.getText();
			return nroCompraEnBacLPU;	
		}

	public void buscarPdeCompra(String btnNombreMenu,String seccionBuscarPdeCompraBAC,String nroDeLPU) 
	{
		List<WebElement> btnBuscarBAC = driver.findElements(By.tagName("li")); 
        	int i;
        		outerloop:
        			for(i = 0; i < btnBuscarBAC.size(); i++)
        	{             
        		if((btnBuscarBAC.get(i).getText().toLowerCase()).contains(btnNombreMenu.toLowerCase()))        		
        		{	                 
        			btnBuscarBAC.get(i).click();
                	
                                             
            List<WebElement> btnSubListaBuscarBAC = btnBuscarBAC.get(i).findElements(By.tagName("li"));
            	for(i = 0; i < btnSubListaBuscarBAC.size(); i++)
            	
            	{ 
            	
            		if((btnSubListaBuscarBAC.get(i).getText().toLowerCase()).contains(seccionBuscarPdeCompraBAC.toLowerCase()))
            		
            		{ 
                    
            			btnSubListaBuscarBAC.get(i).click(); 
                       	espera(3000);
                        break outerloop;
            		}
            	}
        		}
        	}
        	buscarProcesoDeCompra.sendKeys(nroDeLPU);//envia el nro de CDI
        	btnBucarProcesoDeCompra.click();//btn buscar
        	espera(5000);
	}

	public void actoAdministrativoDeAutorizacion()
	{
		btnAcciones.click();
		espera(3000);
		editarProceso.click();//editar proceso (tener en cuenta que se debe arreglar el XPATH para que no pegue x NRO DE <li>
		espera(3000);
		administrativoAutorizacion.click();//Acto Administrativo de Autorizacion
		espera(3000);
		
		 WebElement listaSelect = tipoDNI2;// TIPO DNI
			Select dropdown = new Select(listaSelect);
				   dropdown.selectByVisibleText("Informe"); 
			espera(1000);
			
		
		
		WebElement listaSelect1 = nroEjercicio2;// EJERCICIO
			Select dropdown1 = new Select(listaSelect1);
				   dropdown1.selectByVisibleText("2015"); 
			espera(1000);
			
		
		reparticionMGEYA2.sendKeys("MGEYA");//Reparticion
		espera(1000);
		
		nroReparticion2.sendKeys("03572206");//Numero
		
		nroSADE.click();//seleccionar SADE
		
		btnBuscarActo.click();//btn buscar
		espera(5000);
		
		btnAcciones1.click();//btn opciones/acciones
		espera(1000);
		
		btnVincular1.click();//vincular
		espera(5000);
		
		btnIngresarActo.click();//btn guardar y volver
		espera(3000);
	}

	public void cronogramaBAC(String fila)
	{
		cronogramaItem.click();//CRONOGRAMA
		espera(3000);
		 
		//Fecha y hora estimada de publicación
		estimadaPublicacion.sendKeys(dameDato("Q"+fila));
		horaEstimadaPublicacion.sendKeys(dameDato("R"+fila));
		
//Fecha y hora inicio de consultas
		FechaInicioDeConsultas.sendKeys(dameDato("S"+fila));
		horaInicioDeConsultas.sendKeys(dameDato("T"+fila));
		
// Fecha y hora final de consultas
		FechaFinalDeConsultas.sendKeys(dameDato("U"+fila));
		horaFinalDeConsultas.sendKeys(dameDato("V"+fila));
		
// Fecha y Hora de finalización de Recepción de Ofertas
		FechaFinalDerecepcionDeOfertas.sendKeys(dameDato("W"+fila));
		espera(3000);
		horaFinalDerecepcionDeOfertas.sendKeys(dameDato("X"+fila));
		espera(2000);
								  
/*Fecha y hora recepción de documentación física*/
		
//Fecha y hora inicio recepción de documentación física
		fechaRecepcionDeDocumentacionFisica.sendKeys(dameDato("Y"+fila));
		espera(2000);
		horaRecepcionDeDocumentacionFisica.sendKeys(dameDato("Z"+fila));
		espera(2000); 
		
// Fecha y hora final recepción de documentación física
		fechaFinalDeRecepcionDeDocumentacionFisica.sendKeys(dameDato("AA"+fila));
		espera(2000);
		horaFinalDeRecepcionDeDocumentacionFisica.sendKeys(dameDato("AB"+fila));
		espera(2000);
		
//Fecha y hora inicio recepción de documentación física
		espera(2000);
		horaInicialRecepcion.sendKeys("10");
		
		btnGuardarYvolver.click();
		espera(1000);
		try {
			waitForElementToDisappear(By.id("divPanelFondoTransparente"), 300, driver);
		} catch (Exception e) {
			System.out.println("No se encontro el Elemento: " + e.getMessage());
		}
	
	}
	
	public void actoAdministrativoAlLlamado()
	{
		actoAdministrativoAlLlamado.click();//actoAdministrativoAlLlamado
		espera(3000);
		
		 WebElement listaSelect = tipoDNI2;// TIPO DNI
			Select dropdown = new Select(listaSelect);
				   dropdown.selectByVisibleText("Informe"); 
			espera(1000);
			
		
		
		WebElement listaSelect1 = nroEjercicio2;// EJERCICIO
			Select dropdown1 = new Select(listaSelect1);
				   dropdown1.selectByVisibleText("2015"); 
			espera(1000);
			
		
			reparticionMGEYA2.sendKeys("MGEYA");//Reparticion
		espera(1000);
		
		nroReparticion2.sendKeys("03572206");//Numero
		nroSADE.click();//seleccionar SADE
		btnBuscarActo.click();//btn buscar
		espera(5000);
		btnAcciones1.click();//btn opciones/acciones
		espera(1000);
		btnVincular1.click();//vincular
		espera(5000);
		btnIngresarActo.click();//btn guardar y volver
		espera(30000);
	}
	
	public void estadoDePLiegoPublicado(String nroDeLPU)
	{
		estadoPliegoPublicado.sendKeys(nroDeLPU);
		btnPublicarPliego.click();
		espera(10000);
	}

	public void logInConProveedor() 
	{
		btnIngresarPortal.click();//Btn Ingresar
		nombreUsuario.sendKeys("idelrio");//nombre usuario
		passUsuario.sendKeys("12345678");//contraseña usuario
		btnIngresar.click();//confirmar ingreso de usuario
		
	} 

	public void buscarPdeCompraProveedor(String btnNombreMenu,String seccionBuscarPdeCompraBAC,String nroDeLPU) 
	{
		List<WebElement> btnBuscarBAC = driver.findElements(By.tagName("li")); 
    	int i;
    		outerloop:
    			for(i = 0; i < btnBuscarBAC.size(); i++)
    	{             
    		if((btnBuscarBAC.get(i).getText().toLowerCase()).contains(btnNombreMenu.toLowerCase()))        		
    		{	                 
    			btnBuscarBAC.get(i).click();
            	
                                         
        List<WebElement> btnSubListaBuscarBAC = btnBuscarBAC.get(i).findElements(By.tagName("li"));
        	for(i = 0; i < btnSubListaBuscarBAC.size(); i++)
        	
        	{ 
        	
        		if((btnSubListaBuscarBAC.get(i).getText().toLowerCase()).contains(seccionBuscarPdeCompraBAC.toLowerCase()))
        		
        		{ 
                
        			btnSubListaBuscarBAC.get(i).click(); 
                   	espera(3000);
                    break outerloop;
        		}
        	}
    		}
    	}
    	buscarProcesoDeCompra.sendKeys(nroDeLPU);//envia el nro de CDI
    	btnBusquedaRapida.click();//btn buscar
    	espera(2000);
	} 

	public void adquirirPliegoConProveedor() 
	{
		btnAcciones2.click();//BTN acciones
			espera(3000);
		adquirirPliego.click();//adquirir pliego
			espera(2000);
		adquirirPliegoID.click();//BTN adquirir pliego
			espera(2000);
	
		btnMiEscritorio.click();// volver al escritorio
			espera(1000);
	
	
	}


	/*verificar que de aca para abajo este todo bien*/
	
	
	public void buscarPdeCompraProveedor2(String btnNombreMenu,String seccionBuscarPdeCompraBAC,String nroDeLPU) 
	{
		List<WebElement> btnBuscarBAC = driver.findElements(By.tagName("li")); 
    	int i;
    		outerloop:
    			for(i = 0; i < btnBuscarBAC.size(); i++)
    	{             
    		if((btnBuscarBAC.get(i).getText().toLowerCase()).contains(btnNombreMenu.toLowerCase()))        		
    		{	                 
    			btnBuscarBAC.get(i).click();
            	
                                         
        List<WebElement> btnSubListaBuscarBAC = btnBuscarBAC.get(i).findElements(By.tagName("li"));
        	for(i = 0; i < btnSubListaBuscarBAC.size(); i++)
        	
        	{ 
        	
        		if((btnSubListaBuscarBAC.get(i).getText().toLowerCase()).contains(seccionBuscarPdeCompraBAC.toLowerCase()))
        		
        		{ 
                
        			btnSubListaBuscarBAC.get(i).click(); 
                   	espera(3000);
                    break outerloop;
        		}
        	}
    		}
    	}
    	buscarProcesoDeCompra.sendKeys(nroDeLPU);//envia el nro de CDI
    	btnBusquedaRapida.click();//btn buscar
    	espera(2000);
	}
	
	public void ofertarConProveedor()
	{
		btnAcciones2.click();//BTN acciones
		espera(3000);
		btnOFERTAR.click();//OFERTAR
		espera(2000);
	}
	
	public void ofertaDePliegoStep1()
	 {
		 
		 /*Nombre de la Oferta*/
		nombreDeLaOferta.sendKeys("OFERTA A"); 
		
		/*Descripcion de la Oferta*/
		descripcionDeLaOferta.sendKeys("TEXTO LIBRE");
		
		/*Btn Siguiente paso*/
		btnSiguientePaso.click();
		espera(2000);
		 
	 }
	 
	public void ofertaDePliegoStep2()
	 {
		 
		btnAccionesPliego.click();//acciones
		btnOfertarPliego.click();//ofertar
		espera(3000);
		 
		precioUnitario.sendKeys("100");//precio unitario
		EspecificacionesTecnicas.sendKeys("TEXTO LIBRE");// Especificaciones Tecnicas
		 
		btnGUARDAR.click();//Btn guardar y volver
		espera(7000);
		btnSiguientePaso.click();//Btn siguiente paso
		espera(3000);
		 
	 } 
	 
	public void ofertaDePliegoStep3()
	 {
		btnSiguientePaso.click();//Btn siguiente paso
		 espera(2000);
	 }
	 
	public void ofertaDePliegoStep4()
	 {
		btnSiguientePaso.click();//Btn siguiente paso
		 espera(2000);
	 }
	 
	public void ofertaDePliegoStep5() 
	 {
		chkAceptoCondiciones.click();//check aceptar terminos y condiciones
		lnkConfirmar.click();// confirmar ingreso de oferta
		 
	 }
	
	public void estadosDelPliegoEnApertura(String nroDeLPU)
	 {
		 	 
		 //Pliego -> Publicado
		PliegoEnApertura.sendKeys(nroDeLPU);//envia el nro de CDI en el campo Apertura
		btnPliegoEnApertura.click();//btn pasar a apertura
	     espera(10000);
	        	        
		 
	 }
	
	/*13/08/2018 avance*/
	
	public void evaluacionDeOferta(String btnNombreMenu,String seccionBuscarPdeCompraBAC,String nroDeLPU)
	{
		btnIngresarPortal.click();//Btn Ingresar
		nombreUsuario.sendKeys("pconverso");//nombre usuario
		passUsuario.sendKeys("12345678");//contraseña usuario
		btnIngresar.click();//confirmar ingreso de usuario
		
		List<WebElement> btnBuscarBAC = driver.findElements(By.tagName("li")); 
    		int i;
    			outerloop:
    			for(i = 0; i < btnBuscarBAC.size(); i++)
    			{             
    				if((btnBuscarBAC.get(i).getText().toLowerCase()).contains(btnNombreMenu.toLowerCase()))        		
    				{	                 
    					btnBuscarBAC.get(i).click();
            	
                                         
    					List<WebElement> btnSubListaBuscarBAC = btnBuscarBAC.get(i).findElements(By.tagName("li"));
    					for(i = 0; i < btnSubListaBuscarBAC.size(); i++)
    					{ 
    						if((btnSubListaBuscarBAC.get(i).getText().toLowerCase()).contains(seccionBuscarPdeCompraBAC.toLowerCase()))
    						{ 
    							btnSubListaBuscarBAC.get(i).click(); 
    							espera(3000);
    							break outerloop;
    						}
    					} 
    				}
    			}
    			buscarProcesoDeCompra.sendKeys(nroDeLPU);//envia el nro de CDI
    			btnBucarProcesoDeCompra.click();//btn buscar
    			espera(2000);	
	}

	
	public void aperturaDeOferta() 
	{
		btnAcciones.click();//btn acciones
		espera(3000);
		aperturaDeOferta.click();
	}
	
	public void evaluacionDeOfertaSteps()
	 {
		 
		btnOpciones.click();//opciones
		 espera(2000);
		 evaluacionAdministrativa.click();// evaluacion administrativa
		 EstadoInscripcionSi.click();//CUMPLE
		 FinalizarEvaluacion.click();//Btn finalizar evaluacion
		 espera(2000);
		 
		 
		 
		 btnOpciones.click();//opciones
		 espera(2000);
		 evaluacionTecnica.click();//Evaluacion tecnica
		 btnCheckCumple.click();//CUMPLE
		 FinalizaEvaluacion.click();//Btn finalizar evaluacion
		 espera(2000);
		 
		 
		 btnOpciones.click();//opciones
		 espera(2000);
		 evaluacionEconomica.click();//Evaluacion economica
		 btnCumplebtn.click();//Cumple
		 driver.findElement(By.id("ctl00_CPH1_controlEvaluacionRequisitosMinimos_GrillaRequisito_cell0_4_RadioAccion_0")).click();
		 FinalizaEvaluacion.click();//Btn finalizar evaluacion
		 espera(2000);
		 
	 }

	public void preAdjudicacion() // REFACTORIZAR CODIGO DESDE ACA
	{
	  driver.findElement(By.id("ctl00_CPH1_lnkGenerarDictamen")).click();// Btn Pre Adjudicacion
	  						    
	  driver.findElement(By.id("ctl00_CPH1_txtEncuadreLegal")).sendKeys("Texto Libre2");//encuadre legal
	  driver.findElement(By.id("ctl00_CPH1_txtObservaciones")).sendKeys("texto libre");// observaciones
	  driver.findElement(By.id("ctl00_CPH1_txtAnuncioPreadjudicacion")).sendKeys("texto libre");//preadjudicacion
	  driver.findElement(By.id("ctl00_CPH1_txtExposicion")).sendKeys("texto libre");//exposicion
	  
	  espera(2000);
	  driver.findElement(By.id("ctl00_CPH1_lnkEnviar")).click();//enviar a autorizar
	  
	  espera(5000);
	}

	public void autorizarDictamenes(String btnNombreMenu,String seccionBuscarPdeCompraBAC,String nroDeLPU) 
	{
		List<WebElement> btnBuscarBAC = driver.findElements(By.tagName("li")); 
    	int i; 
    		outerloop:
    			for(i = 0; i < btnBuscarBAC.size(); i++)
    	{             
    		if((btnBuscarBAC.get(i).getText().toLowerCase()).contains(btnNombreMenu.toLowerCase()))        		
    		{	                 
    			btnBuscarBAC.get(i).click();
            	
                                         
        List<WebElement> btnSubListaBuscarBAC = btnBuscarBAC.get(i).findElements(By.tagName("li"));
        	for(i = 0; i < btnSubListaBuscarBAC.size(); i++)
        	
        	{ 
        	
        		if((btnSubListaBuscarBAC.get(i).getText().toLowerCase()).contains(seccionBuscarPdeCompraBAC.toLowerCase()))
        		
        		{ 
                
        			btnSubListaBuscarBAC.get(i).click(); 
                   	espera(3000);
                    break outerloop;
        		}
        	}
    		}
    	}
    	buscarProcesoDeCompra.sendKeys(nroDeLPU);//envia el nro de CDI
    	driver.findElement(By.id("ctl00_CPH1_btnVer")).click();//btn buscar
    	espera(2000);
    	
    	btnAcciones.click();
    	espera(3000);
    	driver.findElement(By.xpath("//*[@id=\'ctl00_CPH1_gridResultado_ctl02_ucAccionesBuscadorGrid_modalBody\']/li/a[text()=\'Autorizar Dictamenes\']")).click();//autorizar dictamenes
    	//driver.findElement(By.xpath("//*[@id=\'ctl00_CPH1_gridResultado_ctl02_ucAccionesBuscadorGrid_modalBody\']/li[3]/a")).click();//autorizar dictamenes
    	espera(3000);
    	
    	driver.findElement(By.id("ctl00_CPH1_txtPassword")).sendKeys("12345678");//contraseña
    	driver.findElement(By.id("ctl00_CPH1_txtJustificacion")).sendKeys("asd");//justificacion en caso de rectificacion
    	espera(2000);
    	driver.findElement(By.id("ctl00_CPH1_lnkAutorizar")).click();//Btn autorizar
    	espera(3000);
    	
	} 

	public void autorizarDictamenesFciorciari(String btnNombreMenu,String seccionBuscarPdeCompraBAC,String nroDeLPU) 
	{
		btnIngresarPortal.click();//Btn Ingresar
		nombreUsuario.sendKeys("fciorciari");//nombre usuario
		passUsuario.sendKeys("12345678");//contraseña usuario
		btnIngresar.click();//confirmar ingreso de usuario
		
		List<WebElement> btnBuscarBAC = driver.findElements(By.tagName("li")); 
    	int i; 
    		outerloop:
    			for(i = 0; i < btnBuscarBAC.size(); i++)
    	{             
    		if((btnBuscarBAC.get(i).getText().toLowerCase()).contains(btnNombreMenu.toLowerCase()))        		
    		{	                 
    			btnBuscarBAC.get(i).click();
            	
                                         
        List<WebElement> btnSubListaBuscarBAC = btnBuscarBAC.get(i).findElements(By.tagName("li"));
        	for(i = 0; i < btnSubListaBuscarBAC.size(); i++)
        	
        	{ 
        	
        		if((btnSubListaBuscarBAC.get(i).getText().toLowerCase()).contains(seccionBuscarPdeCompraBAC.toLowerCase()))
        		
        		{ 
                
        			btnSubListaBuscarBAC.get(i).click(); 
                   	espera(3000);
                    break outerloop;
        		}
        	}
    		}
    	}
    	buscarProcesoDeCompra.sendKeys(nroDeLPU);//envia el nro de CDI
    	driver.findElement(By.id("ctl00_CPH1_btnVer")).click();//btn buscar
    	espera(2000);
    	
    	btnAcciones.click();
    	espera(3000);
    	driver.findElement(By.xpath("//*[@id=\'ctl00_CPH1_gridResultado_ctl02_ucAccionesBuscadorGrid_modalBody\']/li[4]/a")).click();//autorizar dictamenes
    	espera(3000);
    	
    	driver.findElement(By.id("ctl00_CPH1_txtPassword")).sendKeys("12345678");//contraseña
    	driver.findElement(By.id("ctl00_CPH1_txtJustificacion")).sendKeys("asd");//justificacion en caso de rectificacion
    	espera(2000);
    	driver.findElement(By.id("ctl00_CPH1_lnkAutorizar")).click();//Btn autorizar
    	
	} 

	public void autorizarDictamenesAciorciari(String btnNombreMenu,String seccionBuscarPdeCompraBAC,String nroDeLPU) 
	{
		btnIngresarPortal.click();//Btn Ingresar
		nombreUsuario.sendKeys("gguarino");//nombre usuario
		//anteriormente log in con Aciorciari
		passUsuario.sendKeys("12345678");//contraseña usuario
		btnIngresar.click();//confirmar ingreso de usuario
		
		List<WebElement> btnBuscarBAC = driver.findElements(By.tagName("li")); 
    	int i; 
    		outerloop:
    			for(i = 0; i < btnBuscarBAC.size(); i++)
    	{             
    		if((btnBuscarBAC.get(i).getText().toLowerCase()).contains(btnNombreMenu.toLowerCase()))        		
    		{	                 
    			btnBuscarBAC.get(i).click();
            	
                                         
        List<WebElement> btnSubListaBuscarBAC = btnBuscarBAC.get(i).findElements(By.tagName("li"));
        	for(i = 0; i < btnSubListaBuscarBAC.size(); i++)
        	
        	{ 
        	
        		if((btnSubListaBuscarBAC.get(i).getText().toLowerCase()).contains(seccionBuscarPdeCompraBAC.toLowerCase()))
        		
        		{ 
                
        			btnSubListaBuscarBAC.get(i).click(); 
                   	espera(3000);
                    break outerloop;
        		}
        	}
    		}
    	}
    	buscarProcesoDeCompra.sendKeys(nroDeLPU);//envia el nro de CDI
    	driver.findElement(By.id("ctl00_CPH1_btnVer")).click();//btn buscar
    	espera(2000);
    	
    	btnAcciones.click();
    	espera(3000);
    	driver.findElement(By.xpath("//*[@id=\'ctl00_CPH1_gridResultado_ctl02_ucAccionesBuscadorGrid_modalBody\']/li[3]/a")).click();//autorizar dictamenes
    	espera(3000);
    	
    	driver.findElement(By.id("ctl00_CPH1_txtPassword")).sendKeys("12345678");//contraseña
    	driver.findElement(By.id("ctl00_CPH1_txtJustificacion")).sendKeys("asd");//justificacion en caso de rectificacion
    	espera(2000);
    	driver.findElement(By.id("ctl00_CPH1_lnkAutorizar")).click();//Btn autorizar
    	espera(3000);
    	btnMiEscritorio.click();
    	
	} 

	public void publicarDictamenDeAdjudicacion(String btnNombreMenu,String seccionBuscarPdeCompraBAC,String nroDeLPU) 
	{
		List<WebElement> btnBuscarBAC = driver.findElements(By.tagName("li")); 
		int i;
			outerloop:
			for(i = 0; i < btnBuscarBAC.size(); i++)
			{             
				if((btnBuscarBAC.get(i).getText().toLowerCase()).contains(btnNombreMenu.toLowerCase()))        		
				{	                 
					btnBuscarBAC.get(i).click();
        	
                                     
					List<WebElement> btnSubListaBuscarBAC = btnBuscarBAC.get(i).findElements(By.tagName("li"));
					for(i = 0; i < btnSubListaBuscarBAC.size(); i++)
					{ 
						if((btnSubListaBuscarBAC.get(i).getText().toLowerCase()).contains(seccionBuscarPdeCompraBAC.toLowerCase()))
						{ 
							btnSubListaBuscarBAC.get(i).click(); 
							espera(3000);
							break outerloop;
						}
					}
				}
			}
			buscarProcesoDeCompra.sendKeys(nroDeLPU);//envia el nro de CDI
			btnBucarProcesoDeCompra.click();//btn buscar
			espera(2000);	
			
			btnAcciones.click();//btn acciones
			espera(3000);
			driver.findElement(By.xpath("//*[@id=\'ctl00_CPH1_gridResultado_ctl02_ucAccionesBuscadorGrid_modalBody\']/li[3]/a")).click();//publicar dictamen de adjudicacion
			
			//driver.findElement(By.id("ctl00_CPH1_dxedtePublicacion_I")).sendKeys("08/08/2018");//fecha ACA SE PUEDE USAR EL ACELERADOR PARA PASAR EL TIEMPO A PREADJUDICADO
			driver.findElement(By.id("ctl00_CPH1_dxedtePublicacion_B-1")).click();//btn fecha
			espera(1000);
			driver.findElement(By.id("ctl00_CPH1_dxedtePublicacion_DDD_C_BT")).click();//btn HOY
			espera(1000);
			driver.findElement(By.id("ctl00_CPH1_btnPublicarDictamen")).click();//publicar fecha dictamen
			espera(3000);
			/*aca debe ir una espera de mas de 10 min*/ //luego en el siguiente TP buscar el procedimiento de compra
	}

	public void PendientePublicacionAlEstadoPreadjudicado(String nroDeLPU) 
	{
		driver.findElement(By.id("txtNroPliegoEstadoPendPublicacion")).sendKeys(nroDeLPU);
		espera(1000);
		driver.findElement(By.id("btnPasaAEstadoPreadjudicado")).click();//btn preadjudicado
		espera(5000);
		driver.findElement(By.id("txtNroPliegoDisponible")).sendKeys(nroDeLPU);
		espera(1000);
		driver.findElement(By.id("btnPliegoDisponible")).click();//btn pliego disponible
		espera(5000);
	} 
 
	public void generarConvenioMarco(String btnNombreMenu,String seccionBuscarPdeCompraBAC,String nroDeLPU)
	{
		espera(3000);
		btnIngresarPortal.click();//Btn Ingresar
		nombreUsuario.sendKeys("odieguez");//nombre usuario
		passUsuario.sendKeys("12345678");//contraseña usuario
		btnIngresar.click();//confirmar ingreso de usuario
		
		List<WebElement> btnBuscarBAC = driver.findElements(By.tagName("li")); 
    		int i;
    			outerloop:
    			for(i = 0; i < btnBuscarBAC.size(); i++)
    			{             
    				if((btnBuscarBAC.get(i).getText().toLowerCase()).contains(btnNombreMenu.toLowerCase()))        		
    				{	                 
    					btnBuscarBAC.get(i).click();
            	
                                         
    					List<WebElement> btnSubListaBuscarBAC = btnBuscarBAC.get(i).findElements(By.tagName("li"));
    					for(i = 0; i < btnSubListaBuscarBAC.size(); i++)
    					{ 
    						if((btnSubListaBuscarBAC.get(i).getText().toLowerCase()).contains(seccionBuscarPdeCompraBAC.toLowerCase()))
    						{ 
    							btnSubListaBuscarBAC.get(i).click(); 
    							espera(3000);
    							break outerloop;
    						}
    					}
    				}
    			}
    			buscarProcesoDeCompra.sendKeys(nroDeLPU);//envia el nro de CDI
    			btnBucarProcesoDeCompra.click();//btn buscar
    			espera(2000);	
    			btnAcciones.click();
    			espera(3000);
    			driver.findElement(By.xpath("//*[@id=\'ctl00_CPH1_gridResultado_ctl02_ucAccionesBuscadorGrid_modalBody\']/li[3]/a")).click();
    			espera(3000);
    			btnSiguientePaso.click();
    			
    			
    			 WebElement listaSelect = tipoDNI2;// TIPO DNI
    				Select dropdown = new Select(listaSelect);
    					   dropdown.selectByVisibleText("Informe"); 
    				espera(1000);
    				
    			
    			
    			WebElement listaSelect1 = nroEjercicio2;// EJERCICIO
    				Select dropdown1 = new Select(listaSelect1);
    					   dropdown1.selectByVisibleText("2015"); 
    				espera(1000);
    				
    			
    			reparticionMGEYA2.sendKeys("MGEYA");//Reparticion
    			espera(1000);
    			
    			nroReparticion2.sendKeys("03572206");//Numero
    			
    			nroSADE.click();//seleccionar SADE
    			
    			btnBuscarActo.click();//btn buscar
    			espera(5000);
    			
    			btnAcciones1.click();//btn opciones/acciones
    			espera(1000);
    			
    			btnVincular1.click();//vincular
    			espera(5000);
    			
    			driver.findElement(By.id("ctl00_CPH1_ucIngresarActo_btnIngresarActo")).click();//btn finalizar
    			espera(3000);
    			
    			btnCerrarCesion.click();
    		
    			Alert alert = driver.switchTo().alert();
    		 	alert.accept(); 
    		 	
    		 	espera(3000);
    			
    			
	}

	public void recibirConvenioMarco(String btnNombreMenu,String seccionBuscarConvenioMarcoBAC,String nroDeLPU) 
	{
		btnIngresarPortal.click();//Btn Ingresar
		nombreUsuario.sendKeys("idelrio");//nombre usuario
		passUsuario.sendKeys("12345678");//contraseña usuario
		btnIngresar.click();//confirmar ingreso de usuario
		
		
		List<WebElement> btnBuscarBAC = driver.findElements(By.tagName("li")); 
    	int i;
    		outerloop:
    			for(i = 0; i < btnBuscarBAC.size(); i++)
    	{             
    		if((btnBuscarBAC.get(i).getText().toLowerCase()).contains(btnNombreMenu.toLowerCase()))        		
    		{	                 
    			btnBuscarBAC.get(i).click();
            	
                                         
        List<WebElement> btnSubListaBuscarBAC = btnBuscarBAC.get(i).findElements(By.tagName("li"));
        	for(i = 0; i < btnSubListaBuscarBAC.size(); i++)
        	
        	{ 
        	
        		if((btnSubListaBuscarBAC.get(i).getText().toLowerCase()).contains(seccionBuscarConvenioMarcoBAC.toLowerCase()))
        		
        		{ 
                
        			btnSubListaBuscarBAC.get(i).click(); 
                   	espera(3000);
                    break outerloop;
        		}
        	}
    		}
    	}
    	driver.findElement(By.id("ctl00_CPH1_txtNumeroPliego")).sendKeys(nroDeLPU);//envia el nro de CDI
    	driver.findElement(By.id("ctl00_CPH1_btnBuscar")).click();//btn buscar
    	espera(10000);
    	
    	//btnAcciones2.click();//btn acciones
    	espera(3000);
    	
    	driver.findElement(By.id("ctl00_CPH1_gridResultado_cell0_5_UCAccionesConvenioProveedor_imgRecibirConvenioMarco")).click();//btn acciones
    	espera(2000);
    	driver.findElement(By.id("ctl00_CPH1_lnkRecibir")).click();//btn recibir
    	espera(5000);
	}
	
} 
